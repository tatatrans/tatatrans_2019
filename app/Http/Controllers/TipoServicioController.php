<?php

namespace App\Http\Controllers;

use App\TipoServicio;
use Illuminate\Http\Request;

class TipoServicioController extends Controller
{
    public function index(){
    	$tipoServicio = TipoServicio::get();
    	return view('tipoServicio', compact('tipoServicio'));
    }
}
