<?php

namespace App\Http\Controllers;

use App\TipoDevRech;
use Illuminate\Http\Request;

class TipoDevRechController extends Controller
{
    public function index(){
    	$tipoDevRech = TipoDevRech::get();
    	return view('tipoDevRech', compact('tipoDevRech'));
    }
}
