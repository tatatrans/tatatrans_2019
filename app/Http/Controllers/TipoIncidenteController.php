<?php

namespace App\Http\Controllers;

use App\TipoIncidente;
use Illuminate\Http\Request;

class TipoIncidenteController extends Controller
{
   public function index(){
    	$tipoIncidente = TipoIncidente::get();
    	return view('tipoIncidente', compact('tipoIncidente'));
    }
}
