<?php

namespace App\Http\Controllers;

use App\TipoVehiculo;
use Illuminate\Http\Request;

class TipoVehiculoController extends Controller
{
    public function index(){
    	$tipoVehiculo = TipoVehiculo::get();
    	return view('tipoVehiculo', compact('tipoVehiculo'));
    }
}
