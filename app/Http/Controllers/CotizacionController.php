<?php

namespace App\Http\Controllers;

use App\Cotizacion;
use Illuminate\Http\Request;

class CotizacionController extends Controller
{
    public function index(){
    	$cotizacion = Cotizacion::get();
    	return view('cotizacion', compact('cotizacion'));
    }
}
