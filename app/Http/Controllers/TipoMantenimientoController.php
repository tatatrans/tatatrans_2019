<?php

namespace App\Http\Controllers;

use App\TipoMantenimiento;
use Illuminate\Http\Request;

class TipoMantenimientoController extends Controller
{
    public function index(){
    	$tipoMantenimiento = TipoMantenimiento::get();
    	return view('tipoMantenimiento', compact('tipoMantenimiento'));
    }
}
