<?php

namespace App\Http\Controllers;

use App\Ciudad;
use Illuminate\Http\Request;

class CiudadController extends Controller
{
    public function index(){
    	$ciudad = Ciudad::get();
    	return view('ciudad', compact('ciudad'));
    }
}
