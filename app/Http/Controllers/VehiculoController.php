<?php

namespace App\Http\Controllers;

use App\Vehiculo;
use Illuminate\Http\Request;

class VehiculoController extends Controller
{
    public function index(){
    	$vehiculo = Vehiculo::get();
    	return view('vehiculo', compact('vehiculo'));
    }
}
