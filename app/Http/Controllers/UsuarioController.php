<?php

namespace App\Http\Controllers;

use App\Usuario;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class UsuarioController extends Controller
{
    use AuthenticatesUsers;
    protected $loginView = 'usuarios.login';
    protected $guard = 'usuarios';


}
