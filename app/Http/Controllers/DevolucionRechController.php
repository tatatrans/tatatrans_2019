<?php

namespace App\Http\Controllers;

use App\DevolucionRechazo;
use Illuminate\Http\Request;

class DevolucionRechController extends Controller
{
    public function index(){
    	$devolucionRechazo = DevolucionRechazo::get();
    	return view('devolucionRechazo', compact('devolucionRechazo'));
    }
}
