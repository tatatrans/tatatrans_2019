<?php

namespace App\Http\Controllers;

use App\TipoCarga;
use Illuminate\Http\Request;

class TipoCargaController extends Controller
{
    public function index(){
    	$tipoCarga = TipoCarga::get();
    	return view('tipoCarga', compact('tipoCarga'));
    }
}
