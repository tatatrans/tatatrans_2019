<?php

namespace App\Http\Controllers;

use App\Comuna;
use Illuminate\Http\Request;

class ComunaController extends Controller
{
    public function index(){
    	$comuna = Comuna::get();
    	return view('comuna', compact('comuna'));
    }
}
