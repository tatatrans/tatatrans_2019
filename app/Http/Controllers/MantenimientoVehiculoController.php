<?php

namespace App\Http\Controllers;

use App\MantenimientoVehiculo;
use Illuminate\Http\Request;

class MantenimientoVehiculoController extends Controller
{
    public function index(){
    	$mantenimientoVehiculo = MantenimientoVehiculo::get();
    	return view('mantenimientoVehiculo', compact('mantenimientoVehiculo'));
    }
}
