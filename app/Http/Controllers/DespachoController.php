<?php

namespace App\Http\Controllers;

use App\Despacho;
use Illuminate\Http\Request;

class DespachoController extends Controller
{
    public function index(){
    	$despacho = Despacho::get();
    	return view('despacho', compact('despacho'));
    }
}
