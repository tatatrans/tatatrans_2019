<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DevolucionRechazo extends Model
{
    protected $table = 'devoluciones_rechazos';
}
