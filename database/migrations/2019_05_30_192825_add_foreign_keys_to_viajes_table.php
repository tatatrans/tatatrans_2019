<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToViajesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('viajes', function(Blueprint $table)
		{
			$table->foreign('id_despacho', 'id_despacho')->references('id')->on('despachos')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_tipoIncidente', 'id_tipoIncidente')->references('id')->on('tipo_incidentes')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('viajes', function(Blueprint $table)
		{
			$table->dropForeign('id_despacho');
			$table->dropForeign('id_tipoIncidente');
		});
	}

}
