<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDevolucionesRechazosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('devoluciones_rechazos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('descripcion', 500);
			$table->date('fechaDevRech');
			$table->integer('id_tipoDevRech')->index('id_tipoDevRech');
			$table->integer('id_viaje')->index('id_viaje');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('devoluciones_rechazos');
	}

}
