<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateViajesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('viajes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->date('fechaSalida');
			$table->date('fechaLlegadaPunto');
			$table->integer('id_despacho')->index('id_despacho');
			$table->integer('id_tipoIncidente')->index('id_tipoIncidente');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('viajes');
	}

}
