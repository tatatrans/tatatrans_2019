<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDespachosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('despachos', function(Blueprint $table)
		{
			$table->foreign('id_cotizacion', 'id_cotizacion')->references('id')->on('cotizaciones')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('despachos', function(Blueprint $table)
		{
			$table->dropForeign('id_cotizacion');
		});
	}

}
