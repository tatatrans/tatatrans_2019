<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDespachosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('despachos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->date('fechaDespacho');
			$table->integer('id_cotizacion')->index('id_cotizacion');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('despachos');
	}

}
