<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToComunasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('comunas', function(Blueprint $table)
		{
			$table->foreign('id_ciudad', 'id_ciudad')->references('id')->on('ciudades')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('comunas', function(Blueprint $table)
		{
			$table->dropForeign('id_ciudad');
		});
	}

}
