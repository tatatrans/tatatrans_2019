<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVehiculosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('vehiculos', function(Blueprint $table)
		{
			$table->foreign('id_tipoVehiculo', 'id_tipoVehiculo')->references('id')->on('tipo_vehiculos')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('vehiculos', function(Blueprint $table)
		{
			$table->dropForeign('id_tipoVehiculo');
		});
	}

}
