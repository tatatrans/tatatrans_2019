<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCotizacionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cotizaciones', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('numero');
			$table->integer('kilos');
			$table->char('solRetiro', 1);
			$table->integer('id_usuario')->index('id_usuario');
			$table->integer('id_chofer')->index('id_chofer');
			$table->integer('id_tipoCamion')->index('id_tipoCamion');
			$table->integer('id_tipoServicio')->index('id_tipoServicio');
			$table->integer('id_tipoCarga')->index('id_tipoCarga');
			$table->integer('id_comunaOrigen')->index('id_comunaOrigen');
			$table->integer('id_comunaDestino')->index('id_comunadestino');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cotizaciones');
	}

}
