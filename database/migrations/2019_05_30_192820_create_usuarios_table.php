<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsuariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usuarios', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nombre');
			$table->string('apPaterno');
			$table->string('apMaterno');
			$table->string('rut', 10);
			$table->string('direccion');
			$table->string('telefono', 12)->nullable();
			$table->string('email')->unique();
			$table->string('username');
			$table->string('password');
			$table->integer('id_rol')->index('id_rol');			
			$table->rememberToken();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('usuarios');
	}

}
