<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCotizacionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cotizaciones', function(Blueprint $table)
		{
			$table->foreign('id_chofer', 'id_chofer')->references('id')->on('usuarios')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_comunaOrigen', 'id_comunaOrigen')->references('id')->on('comunas')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_comunaDestino', 'id_comunadestino')->references('id')->on('comunas')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_tipoCamion', 'id_tipoCamion')->references('id')->on('tipo_vehiculos')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_tipoCarga', 'id_tipoCarga')->references('id')->on('tipo_cargas')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_tipoServicio', 'id_tipoServicio')->references('id')->on('tipo_servicios')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_usuario', 'id_usuario')->references('id')->on('usuarios')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cotizaciones', function(Blueprint $table)
		{
			$table->dropForeign('id_chofer');
			$table->dropForeign('id_comunaOrigen');
			$table->dropForeign('id_comunadestino');
			$table->dropForeign('id_tipoCamion');
			$table->dropForeign('id_tipoCarga');
			$table->dropForeign('id_tipoServicio');
			$table->dropForeign('id_usuario');
		});
	}

}
