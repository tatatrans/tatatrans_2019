<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMantenimientoVehiculosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mantenimiento_vehiculos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_vehiculo')->index('id_vehiculo');
			$table->integer('id_tipoMantenimiento')->index('id_tipoMantenimiento');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mantenimiento_vehiculos');
	}

}
