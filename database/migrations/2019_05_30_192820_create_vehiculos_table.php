<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVehiculosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vehiculos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('marca');
			$table->string('modelo');
			$table->integer('anio');
			$table->string('combustible');
			$table->integer('cantKm');
			$table->integer('id_tipoVehiculo')->index('id_tipoVehiculo');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vehiculos');
	}

}
