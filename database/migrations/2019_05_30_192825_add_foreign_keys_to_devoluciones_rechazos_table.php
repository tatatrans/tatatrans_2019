<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDevolucionesRechazosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('devoluciones_rechazos', function(Blueprint $table)
		{
			$table->foreign('id_tipoDevRech', 'id_tipoDevRech')->references('id')->on('tipo_dev_rech')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_viaje', 'id_viaje')->references('id')->on('viajes')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('devoluciones_rechazos', function(Blueprint $table)
		{
			$table->dropForeign('id_tipoDevRech');
			$table->dropForeign('id_viaje');
		});
	}

}
