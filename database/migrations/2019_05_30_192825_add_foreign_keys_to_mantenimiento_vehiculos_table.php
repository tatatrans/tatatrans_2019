<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMantenimientoVehiculosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mantenimiento_vehiculos', function(Blueprint $table)
		{
			$table->foreign('id_tipoMantenimiento', 'id_tipoMantenimiento')->references('id')->on('tipo_mantenimientos')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_vehiculo', 'id_vehiculo')->references('id')->on('vehiculos')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mantenimiento_vehiculos', function(Blueprint $table)
		{
			$table->dropForeign('id_tipoMantenimiento');
			$table->dropForeign('id_vehiculo');
		});
	}

}
